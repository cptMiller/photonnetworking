using System.Collections;
using System.Collections.Generic;

using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.Demo.PunBasics;
using UnityEngine.SceneManagement;

using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Miller.PhotonPunTutorial.GameManager
{

    public class GameManager : MonoBehaviourPunCallbacks
    {

        [Tooltip("The Prefab to use for  representing player")]
        public GameObject playerPrefab;

        static public GameManager Instance;

        private GameObject instance;

        private void Awake()
        {

            Instance = this;

            // in case we started this demo with the wrong scene being active, simply load the menu scene
            if (!PhotonNetwork.IsConnected)
            {
                SceneManager.LoadScene("Launcher");

                return;
            }

            if (playerPrefab == null)
            {
                Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
            }
            else
            {
                if (PlayerManager.LocalPlayerInstance == null)
                {
                    Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);
                    // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                    PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
                }
                else
                {
                    Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
                }
            }

        }

        /// <summary>
        /// Color Note : 
        /// - Skyblue : Connection Granted (#87ceeb)
        /// - NeonGreen : Joined room / Joined to master (#39ff14)
        /// - Orange : left room / Disconnected (#FFA500)
        /// - NeonViolet : Joining Room Scene (#B026FF)
        /// </summary>
        #region Unity Callback Method(s) 

        public void LeaveRoom()
        {

            //Leave the current room and return to the Master Server where you can join or create rooms
            PhotonNetwork.LeaveRoom();

        }

        public void QuitApplication()
        {
            Application.Quit();
        }

        void LoadArena()
        {

            if (!PhotonNetwork.IsMasterClient)
            {

                Debug.LogError("<color=#FFA500>PhotonNetwork : Trying to load a level but we're not the Master client </color>");

            }
            else
            {
                Debug.LogFormat("<color=#B026FF>PhotonNetwork : Loading Level : {0}</color>", PhotonNetwork.CurrentRoom.PlayerCount);
                PhotonNetwork.LoadLevel("Room for " + PhotonNetwork.CurrentRoom.PlayerCount); //should be called if we're the master client
            }            

        }

        #endregion


        #region PhotonPun Callback Method(s)

        //watching player connection 
        //every time that player joined, we'll be informed.
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {

            Debug.Log("<color=#39ff14>(OnPlayerEnteredRoom())Now this client was joined this room. Player Name = " + PhotonNetwork.NickName + ", Player Count : " + PhotonNetwork.CurrentRoom.PlayerCount + "</color>");
            Debug.LogFormat("<color=#B026FF>(OnPlayerEnteredRoom()){0}</color>", newPlayer.NickName);//not seen if you're player that connecting

            if (PhotonNetwork.IsMasterClient)
            {

                Debug.LogFormat("<color=#B026FF>(OnPlayerEnteredRoom() IsMasterClient = true) {0}</color>", PhotonNetwork.IsMasterClient);//called before OnPlayerLeftRoom

                LoadArena();

            }

        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {

            Debug.Log("<color=#39ff14>(OnPlayerEnteredRoom())Now this client has left this room. Player Name = " + PhotonNetwork.NickName + ", Player Count : " + PhotonNetwork.CurrentRoom.PlayerCount + "</color>");

            Debug.LogFormat("<color=#B026FF>(OnPlayerLeftRoom()){0}</color>", otherPlayer.NickName);//not seen if you're player that connecting

            if (PhotonNetwork.IsMasterClient)
            {

                Debug.LogFormat("<color=#B026FF>(OnPlayerLeftRoom() IsMasterClient = true) {0}</color>", PhotonNetwork.IsMasterClient);

                LoadArena();

            }

        }

        /// <summary>
        /// Called when the local player left the room. We need to load the launcher scene.
        /// </summary>
        public override void OnLeftRoom()
        {

            //This method wraps loading a level asynchronously and pausing network messages during the process.
            PhotonNetwork.LoadLevel("UIServerControll");

        }

        #endregion


    }

}
