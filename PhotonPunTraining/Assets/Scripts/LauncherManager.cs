using System.Collections;
using System.Collections.Generic;

using Photon.Pun;
using Photon.Realtime;

using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Miller.PhotonPunTutorial.LauncherManager
{

    public class LauncherManager : MonoBehaviourPunCallbacks
    {

        #region Public Variable

        [Header("UI TMPRO")]
        public GameObject UIInputField;
        public GameObject UIConnectingProgress;
        public TMP_InputField nickNameInputField;


        #endregion

        #region Private Variable

        [Header("Important Variable")]
        [SerializeField]
        private string appVersion = "1.0";
        [SerializeField]
        private string playerNickName = "";
        [SerializeField]
        byte maxPlayerPerRoom = 4;

        bool isConnecting;


        #endregion

        #region UnityMethod(s)

        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>
        private void Awake()
        {

            PhotonNetwork.AutomaticallySyncScene = true;

            UIInputField.SetActive(true);
            UIConnectingProgress.SetActive(false);

        }

        /// <summary>
        /// Start the connection process.
        /// - If already connected, we attempt joining a random room
        /// - if not yet connected, Connect this application instance to Photon Cloud Network
        /// Color Note : 
        /// - Skyblue : Connection Granted (#87ceeb)
        /// - NeonGreen : Joined room / Joined to master (#39ff14)
        /// - Orange : left room / Disconnected (#FFA500)
        /// - NeonViolet : Joining Room Scene (#B026FF)
        /// </summary>
        public void Connect()
        {

            UIInputField.SetActive(false);
            UIConnectingProgress.SetActive(true);
            PlayerNameInput();
            //Keep the track of the will to join a game, because when we come back from the game we will get a callback taht we're connected on, so we need to know what to do then.
            isConnecting = true;

            // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
            if (PhotonNetwork.IsConnected)
            {
                // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.
                PhotonNetwork.JoinRandomRoom();
                PhotonNetwork.NickName = this.playerNickName;
            }
            else
            {

                // #Critical, we must first and foremost connect to Photon Online Server.
                PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = this.appVersion;
                PhotonNetwork.NickName = this.playerNickName;
                
            }            

        }

        void PlayerNameInput()
        {

            if (nickNameInputField.text == "")
            {

                playerNickName = "Anonymous_" + Random.Range(0, 100);

            }
            else
            {

                playerNickName = nickNameInputField.text;

            }

        }

        #endregion

        #region PhotonPun Callback Method(s)

        public override void OnConnectedToMaster()
        {
            
            if (isConnecting)
            {

                Debug.Log("<color=#87ceeb>Connection to the Photon Server has been Established.</color>");
                Debug.Log("<color=#FFA500>(OnConnectedToMaster())Player Name : " + PhotonNetwork.NickName + " has joined the Master Servers</color>");
                // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnJoinRandomFailed()
                PhotonNetwork.JoinRandomRoom();

            }

        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
        
            Debug.Log("<color=#FFA500>(OnJoinRandomFailed())No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom</color>");

            // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = maxPlayerPerRoom;

            PhotonNetwork.CreateRoom("MY_ROOM", roomOptions);
        
        }

        public override void OnJoinedRoom()
        {

            Debug.Log("<color=#39ff14>(OnJoinedRoom())Now this client is in a room. Player Name = " + PhotonNetwork.NickName + "(" + PhotonNetwork.LocalPlayer.UserId + ")\n" +
                "PlayerCount : " + PhotonNetwork.CurrentRoom.PlayerCount + "</color>");

            // #Critical: We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                Debug.Log("We load the 'Room for 1' ");


                // #Critical
                // Load the Room Level.
                PhotonNetwork.LoadLevel("Room for 1");
            }

        }

        public override void OnDisconnected(DisconnectCause cause)
        {

            UIInputField.SetActive(false);
            UIConnectingProgress.SetActive(true);
            isConnecting = false;
            Debug.LogWarningFormat("<color=#FFA500>OnDisconnected() was called by PUN with reason {0}</color>", cause);

        }

        #endregion


    }

}