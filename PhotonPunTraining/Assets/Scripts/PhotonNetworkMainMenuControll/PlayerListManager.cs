/// Color Note : 
/// - Skyblue : Connection Granted (#87ceeb)
/// - NeonGreen : Joined room / Joined to master (#39ff14)
/// - Orange : left room / Disconnected (#FFA500)
/// - NeonViolet : Joining Room Scene (#B026FF)
/// - NeonYellow : Ping Connection (#FFF01F)
using System.Collections;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;
using TMPro;

using Photon.Pun;
using Photon.Realtime;

namespace PhotonPunTest.PlayerList
{

    public class PlayerListManager : MonoBehaviourPunCallbacks
    {

        #region PublicVariable

        [Header("UI Game Object")]
        public Transform contentPlayerList;
        public PlayerListing playerListButtonPrefabs;

        #endregion

        #region PrivateVariable
        //[Header("SerializeField Variables")]

        //private bool isPlayerListActive = false;
        /*public bool isListActive
        {
            get { return isPlayerListActive; }
            set
            {
                isPlayerListActive = value;
            }
        }*/

        private List<PlayerListing> cachedPlayerList = new List<PlayerListing>();

        #endregion

        #region UnityMethod(s)


        public void GetCurrentPlayerInRoom()
        {

            if (PhotonNetwork.IsConnectedAndReady)
            {

                foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players)
                {

                    AddPlayerListing(playerInfo.Value);//passing the playerInfo value for listing

                }

            }

        }

        private void AddPlayerListing(Player player)
        {

            if (PhotonNetwork.IsConnectedAndReady)
            {

                int index = cachedPlayerList.FindIndex(x => x._player == player);
                if (index != -1)
                {
                    //update player status / info
                    cachedPlayerList[index].SetPlayerInfo(player);
                }
                else
                {
                    
                    PlayerListing playerList = Instantiate(playerListButtonPrefabs, contentPlayerList);
                    if (playerList != null)
                    {
                        playerList.SetPlayerInfo(player);
                        cachedPlayerList.Add(playerList);
                    }

                }
            }

        }

        #endregion


        #region PhotonPunCallbackMethod(s)

        //Called when a remote player entered the room. This Player is already added to the playerlist.
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {

            AddPlayerListing(newPlayer);

        }

        //Called when a remote player left the room or became inactive. Check otherPlayer.IsInactive.
        public override void OnPlayerLeftRoom(Player otherPlayer)
        {

            contentPlayerList.DestroyChildren();
            cachedPlayerList.Clear();

            GetCurrentPlayerInRoom();

            /*//Lambda expression : get the index which ever listing that has the same Player
            int index = cachedPlayerList.FindIndex(x => x._player == otherPlayer);
            if (index != -1)
            {
                Destroy(cachedPlayerList[index].gameObject);
                cachedPlayerList.RemoveAt(index);
            }*/

        }

        public override void OnLeftRoom()
        {
            contentPlayerList.DestroyChildren();
            cachedPlayerList.Clear();
        }

        #endregion


    }

}