using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using TMPro;
using UnityEngine.UI;

using Photon.Pun;
using Photon.Realtime;

public class PlayerListing : MonoBehaviourPunCallbacks
{

    #region Private Variable

    private string playerName;
    private string playerUserID;
    private bool isPlayerMaster;

    private bool isThisPlayerReady;
    public bool isPlayerReady
    {
        get { return isThisPlayerReady; }
        set { isThisPlayerReady = value; }
    }

    #endregion

    #region Public Variable
    
    [Header("UI and Important Game Object")]
    public TMP_Text playerNameTextField;
    public TMP_Text playerIsMasterTextField;
    public TMP_Text playerUserIdTextField;

    public Player _player { get; private set; }

    #endregion

    #region Unity Method(s)

    #region UnityDefaultMethod(s)

    private void PlayerSettings(string name, string id, bool isMaster)
    {

        playerName = name;
        playerUserID = id;
        isPlayerMaster = isMaster;

        playerNameTextField.text = playerName;
        playerUserIdTextField.text = playerUserID;
        IsPlayerMasterRoom(isPlayerMaster);

    }

    #endregion

    void IsPlayerMasterRoom(bool isMaster)
    {
        if (isMaster)
        {
            playerIsMasterTextField.text = "Game Master";
        }
        else
        {
            playerIsMasterTextField.text = "Game Client";
        }

    }


    public void SetPlayerInfo(Player player)
    {

        _player = player;
        PlayerSettings(_player.NickName, _player.UserId, _player.IsMasterClient); 

    }

    #endregion

}
