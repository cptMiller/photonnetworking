/// Color Note : 
/// - Skyblue : Connection Granted (#87ceeb)
/// - NeonGreen : Joined room / Joined to master (#39ff14)
/// - Orange : left room / Disconnected (#FFA500)
/// - NeonViolet : Joining Room Scene (#B026FF)
/// - NeonYellow : Ping Connection (#FFF01F)

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

using PhotonPunTest.LauncherManager;
using PhotonPunTest.CreateOrJoinRoom;


namespace PhotonPunTest.RoomList
{

    public class RoomListManager : MonoBehaviourPunCallbacks
    {

        #region PublicVariable(s)

        [Header("Important Game Object")]
        public RoomListButton buttonRoomList;
        public Transform contentList;

        #endregion

        #region PrivateVariable(s)
        
        private List<RoomListButton> cachedRoomList = new List<RoomListButton>();

        #endregion


        #region Photon Pun Callback Method(s)

        //update occupancy rate in the rooms
        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {

            foreach (RoomInfo info in roomList)
            {
                //checking if the room has been removed from list
                //removed from roomList
                if (info.RemovedFromList)
                {

                    //Lambda expression : get the index which ever listing that has the same room name
                    int index = cachedRoomList.FindIndex(x => x.roomInformation.Name == info.Name);
                    if (index != -1)
                    {
                        Destroy(cachedRoomList[index].gameObject);
                        cachedRoomList.RemoveAt(index);
                    }

                    Debug.Log("this room info has been removed from list");

                }
                //Added to roomList
                else
                {
                    
                    //Lambda expression : get the index which ever listing that has the same room name
                    int index = cachedRoomList.FindIndex(x => x.roomInformation.Name == info.Name);
                    //if index is -1 (nothing found)
                    if (index == -1)
                    {


                        RoomListButton roomButton = Instantiate(buttonRoomList, contentList);
                        if (roomButton != null)
                        {
                            roomButton.SetRoomInfo(info);
                            cachedRoomList.Add(roomButton);
                        }

                    }
                    else
                    {
                        //modify listing here
                        cachedRoomList[index].SetRoomInfo(info);
                    }

                }

            }

        }

        public override void OnJoinedRoom()
        {

            contentList.DestroyChildren();
            cachedRoomList.Clear();//clear listing everytime we joining room

        }
                

        #endregion

    }

}