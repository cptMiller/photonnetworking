using UnityEngine;

public static class Transforms
{
    //utilities for easily called function when needed
    public static void DestroyChildren(this Transform t, bool destroyImmediately = false)
    {

        foreach (Transform child in t)
        {

            if (destroyImmediately)
            {
                MonoBehaviour.DestroyImmediate(child.gameObject);
            }
            else
            {
                MonoBehaviour.Destroy(child.gameObject);
            }

        }

    }

}
