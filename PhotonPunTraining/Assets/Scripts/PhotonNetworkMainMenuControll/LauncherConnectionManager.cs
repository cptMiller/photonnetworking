/// Color Note : 
/// - Skyblue : Connection Granted (#87ceeb)
/// - NeonGreen : Joined room / Joined to master (#39ff14)
/// - Orange : left room / Disconnected (#FFA500)
/// - NeonViolet : Joining Room Scene (#B026FF)
/// - NeonYellow : Ping Connection (#FFF01F)


using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using TMPro;

using Photon.Pun;
using Photon.Realtime;

using PhotonPunTest.LobbyManager;
using PhotonPunTest.CreateOrJoinRoom;

namespace PhotonPunTest.LauncherManager
{

    public class LauncherConnectionManager : MonoBehaviourPunCallbacks, IConnectionCallbacks
    {

        #region PUBLIC_AND_INTERNAL_VARIABLE
        
        [Header("UI Game Object")]
        public TMP_InputField nameInputField;
        public TMP_Text pingStatusLabel;
        public TMP_Text playerName;
        public GameObject LoginUI;
        public GameObject RoomListUI;

        //[Header("Another Game Object")]
        
        
        #endregion
        #region PRIVATE_AND_PROTECTED_VARIABLE
        
        [Header("SerializeField Private Variable")]
        [SerializeField] private string appVersion = "1";
        [SerializeField] private string playerNickName = "";

        private CustomLobbyManager lobbyManager;
        private MakingOrConnectRoom roomConnectionManager;

        //Unserialized Private / Protected
        //bool isConnected = false;

        #endregion


        #region UNITY_METHOD(S)

        #region UnityCoreMethod(s)

        private void Awake()
        {

            lobbyManager = this.GetComponent<CustomLobbyManager>();
            roomConnectionManager = this.GetComponent<MakingOrConnectRoom>();
            
            //synchronize all clients in the same room sync their level automatically
            PhotonNetwork.AutomaticallySyncScene = true;

            LoginUI.SetActive(true);
            RoomListUI.SetActive(false);
            pingStatusLabel.text = "";
            playerName.text = "";

        }

        private void FixedUpdate()
        {
            //This is how we can check if we're connected to the servers or not
            if (PhotonNetwork.IsConnectedAndReady)
            {
                //Debug.LogWarning("<color=#FFF01F>Ping : " + PhotonNetwork.GetPing() + "</color>");
                pingStatusLabel.text = "<color=#FFF01F>Ping : " + PhotonNetwork.GetPing() + "</color>\n Local time : " + System.DateTime.Now;
            }
        }

        #endregion

        public void ConnectToServer()
        {

            PlayerNameInput();
            // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
            if (!PhotonNetwork.IsConnected)
            {
                ConnectionSettings();
            }

        }

        private void ConnectionSettings()
        {

            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = this.appVersion; //Application Version
            PhotonNetwork.NickName = this.playerNickName; //settingPlayer NickName

        }

        private void PlayerNameInput()
        {

            if (nameInputField.text == "")
            {

                playerNickName = "Anonymous_" + Random.Range(0, 100);

            }
            else
            {

                playerNickName = nameInputField.text;

            }

        }

        #endregion

        #region PHOTONPUN_CALLBACK_METHOD(S)
        //Called to signal that the raw connection got established but before the client can call operation on the server.
        public override void OnConnected()
        {

            Debug.Log("<color=#87ceeb>Server Detected. You're Connected to server! \n Region : " + PhotonNetwork.CloudRegion + " (Raw Connection)(OnConnected()Method)</color>");
            roomConnectionManager.errorMsg.text = "<color=#87ceeb>Server Detected. You're Connected to server! \n Region : " + PhotonNetwork.CloudRegion + " (Raw Connection)(OnConnected()Method)</color>\n";

        }

        //Called when the client is connected to the Master Server and ready for matchmaking and other tasks.
        public override void OnConnectedToMaster()
        {
            
            Debug.Log("<color=#87ceeb>You're Connected to Master Server ! \n Player NickName : " + PhotonNetwork.NickName + " </color>");
            roomConnectionManager.errorMsg.text = "<color=#87ceeb>You're Connected to Master Server ! \n Player NickName : " + PhotonNetwork.NickName + " </color>\n";
            playerName.text = "Welcome : " + PhotonNetwork.NickName;

            lobbyManager.JoinCustomLobby();

            LoginUI.SetActive(false);
            RoomListUI.SetActive(true);

        }

        //Called after disconnecting from the Photon server. It could be a failure or intentional
        public override void OnDisconnected(DisconnectCause cause)
        {
            LoginUI.SetActive(true);
            RoomListUI.SetActive(false);

            Debug.Log("<color=#FFA500>You has been disconnected from server!</color>");
            roomConnectionManager.errorMsg.text =  "<color=#FFA500>You has been disconnected from server!</color>\n";
            
            //isConnected = false;
            roomConnectionManager.isJoin = false;

            playerName.text = "";

        }

        #endregion


    }

}
