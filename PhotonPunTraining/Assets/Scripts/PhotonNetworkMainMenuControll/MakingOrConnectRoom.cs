/// Color Note : 
/// - Skyblue : Connection Granted (#87ceeb)
/// - NeonGreen : Joined room / Joined to master (#39ff14)
/// - Orange : left room / Disconnected (#FFA500)
/// - NeonViolet : Joining Room Scene (#B026FF)
/// - NeonYellow : Ping Connection (#FFF01F)


using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Photon.Pun;
using Photon.Realtime;

using PhotonPunTest.LobbyManager;
using PhotonPunTest.LauncherManager;
using PhotonPunTest.PlayerList;

namespace PhotonPunTest.CreateOrJoinRoom
{

    public class MakingOrConnectRoom : MonoBehaviourPunCallbacks
    {

        #region Public Variable
        [Space(5)]
        [Header("UI TextmeshPro")]
        public TMP_InputField roomNameInput;
        public TMP_InputField maxPlayerInput;
        public Toggle isVisibleInput;
        public Toggle isOpenInput;
        public Toggle isPublishUserIdInput;
        public TMP_Text errorMsg;
        public Button startGameButton;

        public GameObject roomListingPanel;
        public GameObject playerListingPanel;

        #endregion

        #region Private Variable

        [Header("SerializeField Private Variable")]
        private string roomName;
        private byte maxPlayers;
        private bool isVisible;
        private bool isOpen;
        private bool isPublishUserId;

        private LauncherConnectionManager connectionManager;
        private CustomLobbyManager lobbyManager;
        private PlayerListManager playerListManager;

        bool isJoinedroom = false;
        public bool isJoin
        {

            get { return isJoinedroom; }
            set
            {
                isJoinedroom = value;
            }

        }

        #endregion

        #region UNITY_METHOD(S)

        #region UnityCoreMethod(s)
        private void Awake()
        {

            connectionManager = this.GetComponent<LauncherConnectionManager>();
            lobbyManager = this.GetComponent<CustomLobbyManager>();
            playerListManager = playerListingPanel.GetComponent<PlayerListManager>();

            errorMsg.text = "";
        }

        private void Update()
        {

            if (PhotonNetwork.IsMasterClient)
            {

                if (isJoinedroom)
                {
                    startGameButton.interactable = true;
                }
                else
                {
                    startGameButton.interactable = false;
                }

            }
            else
            {
                startGameButton.interactable = false;
            }

        }
        #endregion

        private void CreateRoomNow(string roomName, bool isVisible, bool isOpen, byte maxPlayer, bool isPublishUserId)
        {
            if (!PhotonNetwork.IsConnected)
            {
                return;
            }
            else
            {

                if (!isJoinedroom)
                {
                    
                    RoomOptions roomOptions = new RoomOptions();
                    roomOptions.IsVisible = isVisible;
                    roomOptions.IsOpen = isOpen;
                    roomOptions.MaxPlayers = maxPlayer;
                    roomOptions.PublishUserId = isPublishUserId;
                    //roomOptions.CustomRoomProperties = customRoomProperties;
                    //roomOptions.CustomRoomPropertiesForLobby = propsToListInLobby;
                    PhotonNetwork.CreateRoom(roomName, roomOptions);

                }
                else
                {
                    Debug.Log("You has been joined room! Room Name : " + PhotonNetwork.CurrentRoom.Name);
                }

            }
        }

        public void CreateDefaultRoom()
        {

            if (roomNameInput.text != "" && maxPlayerInput.text != "")
            {

                roomName = roomNameInput.text;
                isVisible = isVisibleInput.isOn;
                isOpen = isOpenInput.isOn;
                maxPlayers = (byte)int.Parse(maxPlayerInput.text);
                isPublishUserId = isPublishUserIdInput.isOn;

            }
            else
            {

                roomName = "Room_" + Random.Range(0, 100);
                isVisible = true;
                isOpen = true;
                maxPlayers = 4;
                isPublishUserId = true;

            }

            CreateRoomNow(roomName, isVisible, isOpen, maxPlayers, isPublishUserId);

        }

        public void StartGameNow(string sceneName)
        {

            if (PhotonNetwork.IsMasterClient)
            {

                if (isJoinedroom)
                {

                    //sceneName = "Room for 1";
                    PhotonNetwork.LoadLevel(sceneName);

                }
                else
                {
                    Debug.LogError("<color=#FFA500>Sorry you're not in a room yet, please join room first!</color>");
                    errorMsg.text = "<color=#FFA500>Sorry you're not in a room yet, please join room first!</color>";
                }

            }
            errorMsg.text = "<color=#FFA500>Sorry you're not a GameMaster !</color>";

        }

        void PlayerListUpdate(bool joinStatus)
        {

            //set the UI from Room List To Player List if player has joined room
            if (joinStatus == true)
            {

                roomListingPanel.SetActive(false);
                playerListingPanel.SetActive(true);

            }
            else
            {
                roomListingPanel.SetActive(true);
                playerListingPanel.SetActive(false);

            }

        }

        public void LeaveRoom()
        {

            PhotonNetwork.LeaveRoom(true);//leaving room to the lobby

        }

        public void DisconnectAction()
        {

            PhotonNetwork.Disconnect();//Disconnected from server

        }

        #endregion

        #region PHOTONPUN_CALLBACK_METHOD(S)

        //Called when this client created a room and entered it. OnJoinedRoom() will be called as well.
        public override void OnCreatedRoom()
        {
            Debug.Log("<color=#B026FF>New room has been created ! </color>");
        }

        //Called when the LoadBalancingClient entered a room, no matter if this client created it or simply joined.
        public override void OnJoinedRoom()
        {

            Debug.Log("<color=#B026FF>Joined Room ! Room Name : " + PhotonNetwork.CurrentRoom.Name + ", MaxPlayers : " + PhotonNetwork.CurrentRoom.MaxPlayers + "</color>");
            connectionManager.playerName.text += "\n<color=#B026FF>Joined Room ! Room Name : " + PhotonNetwork.CurrentRoom.Name + "</color>";
            isJoinedroom = true;

            //change panel to player list
            PlayerListUpdate(isJoinedroom);
            //call the get current player in room function 
            playerListManager.GetCurrentPlayerInRoom();

        }

        //Called when the local user/client left a room, so the game's logic can clean up it's internal state.
        public override void OnLeftRoom()
        {

            Debug.Log("<color=#FFA500>You was left this room !</color>");
            connectionManager.playerName.text += "";
            lobbyManager.JoinCustomLobby();
            isJoinedroom = false;

            PlayerListUpdate(isJoinedroom);

        }

        //Called after disconnecting from the Photon server. It could be a failure or intentional
        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.LogWarning("<color=#FFA500>Room Creation Failed : " + message, this);
        }

        #endregion

    }

}
