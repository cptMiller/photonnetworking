/// Color Note : 
/// - Skyblue : Connection Granted (#87ceeb)
/// - NeonGreen : Joined room / Joined to master (#39ff14)
/// - Orange : left room / Disconnected (#FFA500)
/// - NeonViolet : Joining Room Scene (#B026FF)
/// - NeonYellow : Ping Connection (#FFF01F)

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

using PhotonPunTest.LauncherManager;
using PhotonPunTest.CreateOrJoinRoom;


namespace PhotonPunTest.LobbyManager
{


    public class CustomLobbyManager : MonoBehaviourPunCallbacks//, ILobbyCallbacks
    {

        #region PublicVariable(s)

       

        #endregion

        #region PrivateVariable(s)

        [Header("Serializefield Private Variable")]
        [SerializeField] private string lobbyName;
        enum lobbyTypeList
        {

            Default,
            AsyncRandomLobby,
            SqlLobby

        }
        [SerializeField] private lobbyTypeList lobbyType;
        LobbyType lobbyTypeChoise;

        private LauncherConnectionManager connectionManager;
        private MakingOrConnectRoom roomConnectionManager;

        [Space(5)]
        [Header("Photon Custom Lobby & Room List")]
        private TypedLobby customLobby;
        

        #endregion

        #region UnityMethod(s)

        #region Default Unity Method(s)

        private void Awake()
        {
            connectionManager = this.GetComponent<LauncherConnectionManager>();
            roomConnectionManager = this.GetComponent<MakingOrConnectRoom>();
        }

        #endregion

        public void JoinCustomLobby()
        {
            
            switch (lobbyType)
            {
                case lobbyTypeList.Default:
                    lobbyTypeChoise = LobbyType.Default;
                    Debug.Log("Default LobbyType");
                    break;
                case lobbyTypeList.AsyncRandomLobby:
                    lobbyTypeChoise = LobbyType.AsyncRandomLobby;
                    Debug.Log("AsyncRandomLobby LobbyType");
                    break;
                case lobbyTypeList.SqlLobby:
                    lobbyTypeChoise = LobbyType.SqlLobby;
                    Debug.Log("SqlLobby LobbyType");
                    break;
            }

            customLobby = new TypedLobby(lobbyName, lobbyTypeChoise);

            if (PhotonNetwork.IsConnectedAndReady)
            {
                PhotonNetwork.JoinLobby(customLobby);
            }
        
        }

        public void LeftLobby()
        {

            PhotonNetwork.LeaveLobby();

        }

        #endregion

        #region Photon Pun Callback Method(s)
        //Called on entering a lobby on the Master Server. The actual room-list updates will call OnRoomListUpdate.
        public override void OnJoinedLobby()
        {
        
            //Debug.Log("<color=#B026FF>Welcome to lobby Mr. " + PhotonNetwork.NickName + ", \n Now @ Lobby : " + PhotonNetwork.CurrentLobby.Name + "</color>");
            connectionManager.playerName.text = "<color=#B026FF>Welcome to lobby Mr. " + PhotonNetwork.NickName + ", \n Now @ Lobby : " + PhotonNetwork.CurrentLobby.Name + "</color>";
            roomConnectionManager.errorMsg.text = "Welcome to " + PhotonNetwork.CurrentLobby.Name + " Mr." + PhotonNetwork.NickName + ", Lobby Type : " + PhotonNetwork.CurrentLobby.Type;

        }
        //When you leave a lobby, OpCreateRoom and OpJoinRandomRoom automatically refer to the default lobby.
        public override void OnLeftLobby()
        {

            //Debug.Log("<color=#B026FF>" + PhotonNetwork.NickName + ", \n has left the Lobby, now at Master Server!</color>");
            connectionManager.playerName.text = "Welcome : " + PhotonNetwork.NickName;
            roomConnectionManager.errorMsg.text = "";

        }

        #endregion

    }


}
