using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using TMPro;
using UnityEngine.UI;

using Photon.Pun;
using Photon.Realtime;

public class RoomListButton : MonoBehaviourPunCallbacks
{

    #region Private Variable

    [Header("SerializeField Private Variable")]
    private string roomName;
    public string roomNameGetterAndSetter
    {
        get { return roomName; }
        set
        {
            roomName = value;
        }
    }
    private bool isVisible;
    private bool isOpen;
    private byte maxPlayers;
    private int playerTotal;

    private bool isThisFull = false;
    public bool isThisRoomFull{
        get { return isThisFull; }
        set
        {
            isThisFull = value;
        }
    }

    public RoomInfo roomInformation
    {
        get; private set;
    }

    #endregion

    #region Public Variable
    [Header("UI and Important Game Object")]
    public TMP_Text roomNameText;
    public TMP_Text roomSizeText;
    public TMP_Text roomPingText;
    #endregion

    #region Unity Method(s)

    #region UnityDefaultMethod(s)

    private void Update()
    {

        IsRoomFull();

    }

    #endregion

    public void RoomList(string name, bool visibility, bool openStatus, byte maximumPlayers, int playerCount)
    {

        roomName = name;
        isVisible = visibility;
        isOpen = openStatus;
        maxPlayers = maximumPlayers;
        playerTotal = playerCount;

    }

    public void JoiningRoom()
    {

        if (!PhotonNetwork.InRoom)
        {
            //if the targeted room is still have empty slot
            Debug.Log("Joining Room");
            PhotonNetwork.JoinRoom(roomName);

        }
        else
        {
            Debug.Log("Has been Inside This Room, Can't Change Room !");
        }

    }

    void IsRoomFull()
    {

        Button buttonScript = GetComponent<Button>();

        if (playerTotal >= maxPlayers)
        {
            buttonScript.interactable = false;
        }
        else
        {
            buttonScript.interactable = true;
        }

    }

    public void SetRoomInfo(RoomInfo roomInfo)
    {

        roomInformation = roomInfo;

        roomNameText.text = roomInfo.Name;
        roomSizeText.text = "Room Size : " + roomInfo.PlayerCount + " / " + roomInfo.MaxPlayers;
        roomPingText.text = "Ping : " + PhotonNetwork.GetPing() + " /ms";

        RoomList(roomInfo.Name, roomInfo.IsVisible, roomInfo.IsOpen, roomInfo.MaxPlayers, roomInfo.PlayerCount);

    }

    #endregion

}
